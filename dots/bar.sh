#!/bin/sh
while sleep 1; do 
   read -r bat_cap < /sys/class/power_supply/BAT0/capacity
   read -r bat_stat < /sys/class/power_supply/BAT0/status
   windowname=$(xdotool getwindowfocus getwindowname)
   date=$(date "+%r")

   echo "%{c}$windowname" "%{l}$date" "%{r}$bat_cap $bat_stat"
done
